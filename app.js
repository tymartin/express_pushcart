const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const passport = require('passport'); 
const cors = require('cors'); 

//set up server

//initialize the app

const app = express();

const port = 3000;

//connect to database
mongoose.connect(
	'mongodb://localhost:27017/myapp',
	 {
	 	useNewUrlParser: true,
	 	useUnifiedTopology: true,
	 	
	 });
mongoose.connect('connected', () => {
	console.log("Database connected for real");
})


//use dependencies/middleware
app.use(bodyParser.json());

app.use((req,res,next) => {
	console.log(req.body)
	next()
})

app.use(passport.initialize())
app.use(cors())


// Routes

app.use('/categories', require('./routes/categories'));
app.use('/products',require('./routes/products'));
app.use('/users',require('./routes/users'));
app.use('/transactions',require('./routes/transactions'));


//error handling middleware
app.use(function(err,req,res,next) {
		res.status(422).json({
				error : err.message,
		});
		// console.log(err);
})


app.listen(port, () => {
	console.log(`Listening to port ${port}`);
})

