const express = require('express');
const router = express.Router();

const CategoryModel = require('../models/Categories');

//index
router.get('/', function(req,res){
	// res.json({message : "This is CATEGORIES INDEX"});

	CategoryModel.find().then(
		categories => {
			res.json(categories)
		}).catch(next)
});

// router.use(myfunction);

// let myfunction = function(req,res,next) {
// 	if(req.bodyImage != null) {
// 		next();
// 	} else {
// 		res.status(403).json({
// 			message : "Unauthorized request"
// 		});
// 	}
// 	console.log('My Request');
	
// }

// router.use(myfunction);

//single
router.get('/:id', (req,res, next) => {
	// res.json({
	// 	// data : "This is GET single",
	// 	data : req.params.id	
	// });
	CategoryModel.findOne({
		_id : req.params.id
	}).then(
			category => res.json(category)).catch(next);
});


//create
router.post('/', (req,res, next) => {
	// console.log(req.body)

	// CategoryModel.create({ name: 'small' }, function (err, small) {
	// 		console.log(small);
	// 		res.send(small);
  // if (err) return handleError(err);
  // saved!
// });

	// res.json({
	// 	data : req.body,	
	// });


	CategoryModel.create({
		name: req.body.name,
		// image: "",
		// age: ""
	}).then( (category) => {
		res.send(category)
	}).catch(next)
})

//put
router.put('/:id', (req,res, next) => {
	// res.json({
	// 	data : "this is a PUT request In CATEGORIES",
	// 	id : req.params.id,
	// 	body : req.body
	// })

	console.log(req.body.name)

	CategoryModel.findOneAndUpdate({
		_id : req.params.id
	},
	{
		name : req.body.name
	},
	{
		new : true

	}).then(category => res.json(category))
		.catch(next)

})

//delete
router.delete('/:id', (req,res, next) => {
	// res.json ({
	// 	data : "this is a DELETE request IN CATEGORIES",
	// 	id : req.params.id + " to be deleted"
	// })

	CategoryModel.findOneAndDelete({
		_id : req.params.id
	}).then(category => res.json(category))
		.catch(next)
})

module.exports =  router;
