const express = require('express')
const router = express.Router();
const Product = require('./../models/Products')
const passport = require('passport');
const Transaction = require('./../models/Transactions'); 
const auth = require('./../auth'); 

router.post('/orders', (req,res,next) => {
 let orders = req.body.orders; 

    let orderIds = orders.map( product => {
        return product.id
    })

   // res.send(req.body)
    Product.find({_id : orderIds})
    .then(products => {
        let total = 0;

        let newProducts = products.map( product => {
            let matchedProduct = {};
            orders.map( order => {
                if (product.id === order.id) {
                    matchedProduct = {
                        productId : product._id,
                        name : product.name,
                        price : product.price,
                        image : product.image,
                        quantity : order.qty,
                        subtotal : order.qty * product.price
                    }
                }
            })
            total += matchedProduct.subtotal
            return matchedProduct;
        })
        res.send({
            products : newProducts,
            total 
        })
    }) 
})


router.post('/',passport.authenticate('jwt', {session : false}),(req,res,next) => {
  
    let orders = req.body.orders; 

    let orderIds = orders.map( product => {
        return product.id
    })

   // res.send(req.body)
    Product.find({_id : orderIds})
    .then(products => {
        let total = 0;

        let newProducts = products.map( product => {
            let matchedProduct = {};
            orders.map( order => {
                if (product.id === order.id) {
                    matchedProduct = {
                        productId : product._id,
                        name : product.name,
                        price : product.price,
                        image : product.image,
                        quantity : order.qty,
                        subtotal : order.qty * product.price
                    }
                }
            })
            total += matchedProduct.subtotal
            return matchedProduct;
        })
        let transaction = {
             userId: req.user._id,
            transactionCode: Date.now(),
            total, 
            products: newProducts
         }
         Transaction.create(transaction)
         .then(transaction => {
        return res.send(transaction)           
         })
    })

})

router.put('/:id', passport.authenticate('jwt', {session:false}), auth ,(req,res,next) => {
    Transaction.findByIdAndUpdate(req.params.id,{ status : req.body.status}, { new : true})
    .then(transaction => res.send (transaction))
})



module.exports = router;