const express = require('express');
const router = express.Router();
const User = require("../models/Users");
const bcrypt = require('bcrypt')
const passport = require('passport');
require('./../passport-setup');
const jwt = require('jsonwebtoken');



router.post('/register', (req,res,next) => {
	// res.send("This is register endpoint")


	// get the details
	let firstname = req.body.firstname;
	let lastname = req.body.lastname;
	let email = req.body.email;
	let password = req.body.password;
	let confirmPassword = req.body.confirmPassword;

	// check the completeness
	if(!firstname || !lastname || !email || !password || !confirmPassword){
		return res.status(400).send({
			message: "Incomplete fields"
		})
	} else {
		
		// check password if not less than 8chars
		if(password.length < 8){
			return res.status(400).send({
				message: "Password is too short"
			})
		}
	}

	// check if password and confirm pass is same
	if(password !== confirmPassword){
		return res.status(400).send({
			message: "Password and Confirm Password is not same"
		})
	}

	// check if email already exist
	User.findOne({email: email})
	.then(user => {
		if(user){
			return res.status(400).send({
				message: "Email already in use"
			})
		}else{
			const saltRounds = 10;

			bcrypt.genSalt(saltRounds, function(err,salt){
				bcrypt.hash(password, salt, function(err,hash){
					User.create({
						firstname, 
						lastname, 
						email, 
						password : hash
					})
					.then(user => {
						return res.send(user)
					})
				})
			})
		}
	})
	.catch(next)

})

router.post('/profile', passport.authenticate('jwt', {session:false}), (req,res) => {
	res.send(req.user)
})

router.post('/login', (req,res,next)=> {
	let email = req.body.email;
	let password = req.body.password;

	// check if there are credentials

	if(!email || !password){
		return res.status(400).send({
			message: "Something went wrong"
		})
	}

	// check if registered
	User.findOne({email})
	.then( user => {
		// if there are no email matched
		if(!user){
			return res.status(400).send({
				message: "User does not exist"
			})
		}else{
			// check if password matched with email holder
			bcrypt.compare(password, user.password, (err,passwordMatched) => {
				if(passwordMatched){
					let token = jwt.sign({id: user._id}, 'secret');
					return res.send({message: "Login Successful",token : token})
				}else{
					return res.send({message: "Login Failed"})
				}
			})
		}
	})
	.catch(next)
})





module.exports = router;





