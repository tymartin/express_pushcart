const express = require('express');
const router = express.Router();

const ProductModel = require('../models/Products');


//index
router.get('/', function(req,res, next){
	ProductModel.find().then(
		products => {
			res.json(products)
		}).catch(next)
});

//single
router.get('/:id', (req,res, next) => {
	ProductModel.findOne({
		_id : req.params.id
	}).then(
			product => res.json(product)).catch(next);
});


//create
router.post('/', (req,res, next) => {
	ProductModel.create({
		name: req.body.name,
		categoryId: req.body.categoryId,
		price: req.body.price,
		description: req.body.description,
		image: req.body.image
	}).then(
			product => res.send(product)).catch(next);


})

//put
router.put('/:id', (req,res, next) => {
	ProductModel.findOneAndUpdate({
		_id : req.params.id
	},
	{
		name: req.body.name,
		categoryId: req.body.categoryId,
		price: req.body.price,
		description: req.body.description,
		image: req.body.image
	},

	{
		new : true

	}).then(product => res.json(product))
		.catch(next)
})

//delete
router.delete('/:id', (req,res, next) => {
	ProductModel.findOneAndDelete({
		_id : req.params.id
	}).then(product => res.json(product))
		.catch(next)
})

module.exports =  router;
