const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const TransactionSchema = new Schema({
	
	userId : {
		type: String,
		required: [true, "UserId field required"]
	},
	createdAt : {
		type: Date,
		default: Date.now
	},
	transactionCode : {
		type: String,
		required: [true, "Transaction Code required"]
	}, 
	status : {
		type: String,
		required: [true, "Status required"],
		default : "Pending"
	}, 
	paymentMode : {
		type : String, 
		default : "Over the Counter"
	}, 
	products : [{
		productId : String, 
		quantity : Number, 
		subtotal : Number
	}], 
	total : {
		type: Number, 
		required : true 
	}

});

module.exports = mongoose.model('Transaction', TransactionSchema);


