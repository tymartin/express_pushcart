const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const UserSchema = new Schema ({
	firstname : {
		type: String,
		required: [true, "User firstname field required"]
	},
	lastname : {
		type: String,
		required: [true, "User lastname field required"]
	},
	email : {
		type: String,
		required: [true, "User email field required"],
		unique: true
	},
	password : {
		type: String,
		required: [true, "User password field required"]
	},
	dateCreated : {
		type: Date,
		default: Date.now
	},
	role : {
		type: String,
		required: [true, "User role field required"],
		default: "User"
	}

})


module.exports = mongoose.model('User', UserSchema);